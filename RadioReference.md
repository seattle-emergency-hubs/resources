# 2-Way Radio

## Programming
We recommend using CHIRP, a free and cross-platform app that supports hundreds of radios. You will need to install this software on a computer, and connect the radio using a Kenwood K1 style adapter.

If you cloned this repository, you can access the files in "Radio Programs" directly. If not, download them from [Radio Programs](Radio%20Programs) for the appropriate radio(s).

