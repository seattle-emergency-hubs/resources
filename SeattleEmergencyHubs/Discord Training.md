## What do HubCaps want to know?
* What is Discord?
* What is a Discord server?
* Is Discord safe?
* Who can see what I post?

## Quick-Start Topics
* Creating an account
* Joining our server
* Installing the applications
* Setting notification preferences
* Participating in discussions

## Deep Dive Topics
* Managing private channels
* *