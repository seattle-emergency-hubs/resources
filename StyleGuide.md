# Seattle Emergency Hubs Style Guide
**This document is a work in progress, and has not been officially adopted*

## Introduction

Welcome to the Seattle Emergency Hubs Style Guide! This document is designed to help all our volunteers and staff create consistent, professional, and on-brand materials. Whether you're drafting a document, designing a presentation, or creating digital content, this guide will provide you with the necessary guidelines to ensure our brand is accurately represented.

## Our Mission

"The Seattle Hub Network is a coalition of separate Seattle neighborhood hubs having the shared mission of preparing for, responding to, and being resilient after a major disaster.

We accomplish this mission through advocating in support of Hubs, creating opportunities to develop and share understanding of best practices, sponsoring activities such as training, drills and meetings which benefit all, and assisting each other in our individual hub efforts."

## Brand Identity

### Logo Usage

- **Primary Logo**: [Describe the primary logo, including its colors and any variations. Include instructions on how to access the correct versions.]
- **Secondary Logo**: [If applicable, describe any secondary logos and how they should be used.]
- **Size and Placement**: Provide guidelines on the minimum size, scaling, and preferred placement of the logo on different types of materials.
- **Do's and Don'ts**: Clearly outline what alterations (if any) are permitted, such as resizing, recoloring, or rotating the logo.

### Color Palette

- **Primary Colors**: List your primary colors along with their Pantone, CMYK, RGB, and HEX codes to ensure accurate reproduction across print and digital mediums.
- **Secondary Colors**: If you have secondary colors, list them here with their corresponding codes.
- **Usage**: Provide examples of how these colors should be used in materials, including which colors are preferred for backgrounds, text, and accents.

### Typography

- **Primary Font**: Specify the primary font(s) for your organization, including where it can be downloaded or purchased, if necessary.
- **Secondary Font**: If you have a secondary font, provide details here.
- **Usage Guidelines**: Explain when and how to use each font type, including guidelines for headings, body text, and any other specific uses like captions or pull quotes.

### Imagery and Graphics

- **Style**: Describe the style of imagery and graphics that align with your brand (e.g., professional, candid, illustrative).
- **Usage**: Provide guidelines on how and where to use imagery, including any restrictions or preferred subjects.
- **Sources**: If you have preferred sources for images or a repository of approved images, include that information here.

## Document Formatting

### Google Docs

- **Template**: If you have a Google Docs template, explain how to access and use it.
- **Font Size and Style**: Provide specific instructions for headings, subheadings, and body text.
- **Color Usage**: Explain how to use your brand colors within documents.

### Microsoft Word

- **Template**: Provide information on how to access and use any Microsoft Word templates.
- **Font Size and Style**: Detail the font sizes and styles for different sections of a document.
- **Color Usage**: Guide users on how to incorporate your brand colors.

### PDFs

- **Purpose**: PDFs should be used for creating "print-ready" documents that are finalized and will not require further edits. This ensures that our materials maintain their formatting and are accessible to everyone.
- **Creating PDFs**: Offer instructions or tips for converting documents to PDFs, ensuring they maintain formatting and are accessible.
- **Accessibility**: Provide guidelines to ensure PDFs are accessible to people with disabilities, such as using alt text for images.

## Communication Guidelines

- **Tone and Voice**: Describe the tone and voice appropriate for Seattle Emergency Hubs' communications. This might vary depending on the audience (e.g., formal for grant proposals, conversational for social media).
- **Email Signature**: Provide a standard template for email signatures, including how to incorporate logos and which contact information to include.

## Additional Resources

- **Contact Information**: Include contact information for individuals or departments responsible for brand management, in case volunteers have questions.
- **FAQs**: If there are common questions about your style guide, answer them here.

## Conclusion

Thank you for adhering to the Seattle Emergency Hubs Style Guide. By following these guidelines, you help maintain the integrity of our brand and support our mission. If you have any questions or need further clarification, please don't hesitate to reach out to [Contact Information].
